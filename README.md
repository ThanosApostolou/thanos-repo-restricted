# thanos-repo-restricted

Thanos repository for Fedora 30. Only for packages that with restricted licenses which cannot be built in copr (https://copr.fedorainfracloud.org/coprs/thanosapostolou/thanos-repo). Spec files are available at https://gitlab.com/ThanosApostolou/fedora-specs.

## Use this repository
Enable repository with
```
sudo dnf install https://thanosapostolou.gitlab.io/thanos-repo-restricted/30/x86_64/thanos-repo-restricted-release-30-1.fc30.noarch.rpm
```

## List of Packages:
  - megasync
  - megasync-nautilus
  - megasync-nemo
  - thanos-repo-restricted-release

## Build packages yourself
1. Configure rpmfusion and install **mock-rpmfusion-free**
2. Clone respository with the specs with `git clone https://gitlab.com/ThanosApostolou/fedora-specs`
3. cd fedora-specs
4. Download sources with `spectool -g SPECS/PACKAGE.spec`
5. Build srpm with `mock -r fedora-30-x86_64-rpmfusion_free --buildsrpm --spec=SPECS/PACKAGE.spec --sources=.`
6. Copy srpm from **/var/lib/mock/fedora-30-x86_64/result** to your home folder
7. Build the package with `mock -r fedora-30-x86_64-rpmfusion_free ~/PACKAGE.srpm`

## Push packages
1. Copy packages built with mock at the appropriate folder (debug, SRPMS, x86_64).
2. For each folder debug, SRPMS, x86_64 run
```
	createrepo_c --zck --update .
```
3. Push and then merge to stable branch.
